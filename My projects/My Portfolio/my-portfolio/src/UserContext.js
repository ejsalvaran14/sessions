import React from 'react';

//Creates a context object for sharing data between components
const UserContext = React.createContext(); // Dito masstore yung gusto nating ishare para magamit si user,setUser,unsetUser function sa lahat ng routes

// UserProvider allows to share context object, for components/pages to use
// para magkalaman si UserContext, magpprovide si UserProvider
export const UserProvider = UserContext.Provider;

export default UserContext;