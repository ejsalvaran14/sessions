import {useState, useEffect} from 'react'
import {UserProvider} from './UserContext'
import {BrowserRouter as Router} from 'react-router-dom'
import {Routes, Route} from 'react-router-dom'
import {Container} from 'react-bootstrap'
import './App.css';
import AppNavbar from './components/AppNavbar'



function App() {
 
  // Gets the token from local storage and assignes the token to user using useState
  // bale ang initial value ng user is yung token
  const [user, setUser] = useState({
    //token: localStorage.getItem('token')
    id: null,
    isAdmin: null
  });

  //unsetUser is a function for clearing local storage
  const unsetUser = () => {
    localStorage.clear();
  }

  //Check if user info is properly and the localStorage if cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  },[user])

  return (
    // The user state, setUser (setter function) and unsetUser function is provided in the UserContext to be shared to other pages/components
    // UserProvider- shineshare niya yung mga values sa mga route/pages
    <UserProvider value = {{user, setUser, unsetUser}}>
    <Router>
    <AppNavbar />
      <Container>
        
        <Routes> {/* Used to handle multiple routes. Route are used for assigning a path*/}
          
        </Routes>
      </Container>
    </Router>
    </UserProvider>
    
  );
}

export default App;
