# 230714 - Batch 297 (Ms. Cam):  Academic Reading List 


### **Topics**

- JavaScript Array Methods
  - [tutorial](https://www.javascripttutorial.net/javascript-array-methods/)
- ES6 Updates
  - [w3schools](https://www.w3schools.com/js/js_es6.asp)
  - [exploringjs](https://exploringjs.com/es6/ch_overviews.html)
- Selection Control
  - [MDN Docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/if...else)
- For Loop Statements
  - [MDN Docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for)
- JS DOM Manipulation
  - [jstutorial-dom](https://www.javascripttutorial.net/javascript-dom/)
  - [odinproject](https://www.theodinproject.com/lessons/foundations-dom-manipulation-and-events#exercise)
  - [medium](https://medium.com/@johnwadelinatoc/manipulating-the-dom-with-fetch-7bfddf9c526b)
  -[jstutorial-form](https://www.javascripttutorial.net/javascript-dom/javascript-form-validation/)



### **Purpose**
- Review and practive some array methods
- Create Javascript functions using the arrow function syntax, perform array and object destructuring.
- Create JavaScript for statements
- Understand the Document Object Model in JS and apply the learned concepts by following the steps in making JS DOM projects such as Word Counter and Toggle Password Visibility


### **Goal to Checking**

#### In your Academic Break Reading List Folder, create a folder named 230714:

1. Try these activities to practice your skills in array manipulation/methods<br>

    a. Create an array-practice folder in the 230714 folder<br>
    b. Create your index.html and index.js inside of it<br>
    c. Do these activities inside the index.js file:<br>

    1. Write a simple JS function to join all elements of the following array into a string:
          - color = ["Blue","Red","Yellow","Orange","Green","Purple"]
    2. Create a function that will add a planet at the beginning of the array:
          - planet = ["Venus","Mars","Earth"]
    3. Create an array of your favorite singers' names. Create a function that will remove the last name in the array. Then, replace the deleted name with other two names.

    d. Send a screenshot of your work's console in the Batch 297 Thread for the Reading List Activities<br>

2. Create a student grading system (Selection Control and ES6 Updates)<br>
    a. Create an grading-function folder in the 230714 folder<br>
    b. Create your index.html and index.js inside of it<br>
    c. Create a student grading system using an arrow function that will receive the grade from a prompt. The grade categories are as follows:<br>
         - Novice (80-below)
         - Apprentice (81-90)
         - Master (91-100)

    d. Send a screenshot of your work's console in the Batch 297 Thread for the Reading List Activities<br>

3. Create an odd-even checker (For Loop)<br>
    a. Create an odd-even-checker folder in the 230714 folder<br>
    b. Create your index.html and index.js inside of it<br>
    c. Create an odd-even checker function that will check which numbers from 1-50 are odd and which are even.<br>

    Sample output in the console: <br>
        1 - The number 1 is odd <br>
        2 - The number 2 is even <br>
        3 - The number 3 is odd <br>

    d. Send a screenshot of your work's console in the Batch 297 Thread for the Reading List Activities<br>

4. Create a word counter (DOM Manipulation)<br>

    a. Create a word-counter folder in the 230714 folder<br>
    b. Create your index.html and index.js inside of it<br>
    c. Follow the step by step guide in creating a Word Counter App using the resource given<br>
    d. Send a screenshot of your work in the Batch 297 Thread for the Reading List Activities<br>

5. Create a Toggle Password Visibility Feature (DOM Manipulation)<br>

    a. Create a visibility-toggle folder in the 230714 folder<br>
    b. Create your index.html and index.js inside of it<br>
    c. Follow the step by step guide in creating the Toggle Password Visibility Feature using the resource given<br>
    d. Send a screenshot of your work in the Batch 297 Thread for the Reading List Activities<br>