import AppNavbar from './components/AppNavbar';
import Courses from './pages/Courses';
import Home from "./pages/Home";
//Bootstrap

//BrowserRouter, Routes, Route works together to setup components endpoints
import {BrowserRouter as Router} from 'react-router-dom'
import {Routes, Route} from 'react-router-dom'
import {useState, useEffect} from 'react';
import { Container } from 'react-bootstrap';
import './App.css';

import  Register  from "./pages/Register"
import  CourseView  from "./pages/CourseView"
import Login from "./pages/Login"
import Logout from "./pages/Logout"
import Error from "./pages/Error"
import Profile from "./pages/Profile"
import AdminView from "./pages/AdminView"

import {UserProvider} from "./UserContext"
function App() {


  // Gets the token from local storage and assignes the token to user using useState
  // bale ang initial value ng user is yung token
  const [user, setUser] = useState({
    //token: localStorage.getItem('token')
    id: null,
    isAdmin: null
  });

  //unsetUser is a function for clearing local storage
  const unsetUser = () => {
    localStorage.clear();
  }

  //Check if user info is properly and the localStorage if cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  },[user])

  return (
    // The user state, setUser (setter function) and unsetUser function is provided in the UserContext to be shared to other pages/components
    // UserProvider- shineshare niya yung mga values sa mga route/pages
    <UserProvider value = {{user, setUser, unsetUser}}>
    <Router>
      <Container>
        <AppNavbar />
        <Routes> {/* Used to handle multiple routes. Route are used for assigning a path*/}
          <Route path ="/" element = {<Home/>} />
          <Route path ="/courses" element = {<Courses/>} />
          <Route path = "/login" element = {<Login/>} />
          <Route path = "/register" element = {<Register/>} />
          <Route path = "/courses/:courseId" element = {<CourseView/>} />
          <Route path = "/adminView" element = {<AdminView/>} />
          <Route path = "/logout" element = {<Logout/>} />
          <Route path = "/*" element = {<Error/>} />
          <Route path = "/Profile" element = {<Profile/>} />
        </Routes>
      </Container>
    </Router>
    </UserProvider>


    /* <>
      <AppNavbar />
      <Container>
        <Home />
        <Courses />
        <Register/>
        <Login />
      </Container>

    </> */
    
  );
}

export default App;
