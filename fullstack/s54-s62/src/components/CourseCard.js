import { Card, Button } from 'react-bootstrap';
import coursesData from '../data/coursesData';
import {useState} from 'react';
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'


export default function CourseCard({courseProp}) {
    //console.log(props);//for checking to see if data was successfully passed 
    //console.log(typeof props);// Every component recieves information in a form of an object

    const{ _id, name, description, price } = courseProp;

    // Use the state hook in this component to be able to store its state
    /* 
        Syntax:
            const [getter, setter] = useState(initialGetterValue);
     */
   /*  const [count,setCount] = useState(0)
    const maxCount = 30;
    

    console.log(useState(0)); */

    // Function that keeps track of the enrolles for a course

    /* function enroll (){
        if(count < maxCount){
            setCount(count +1)
            console.log("Enrollees: "+ count)
        }else{
            window.alert("No more seats");
        }
    }; */

	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
            {/* <Card.Text>Enrollees : {count}</Card.Text> */}
	        {/* <Link className ="btn btn-primary" to={`/courses/${_id}`}>Details</Link> */}
            <Link variant = "primary" to ={`/courses/${_id}`}><Button>Details</Button></Link>
	    </Card.Body>
	</Card>	
	)
}


//Check if the CourseCard Component is getting the correct prop types
CourseCard.propTypes = {
    course: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}