import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
    return(
        <Row className="mt-3 mb-3">
            <Col xs={12} md={4}>
                <Card className = "cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>Learn from Home</h2>
                    </Card.Title>
                    <Card.Text>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam vitae nesciunt eos similique. Eum expedita dolor inventore natus dolorem blanditiis sapiente unde, temporibus facilis quos delectus eligendi eveniet, mollitia ipsa.
                    </Card.Text>
                </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className = "cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>Study Now, Pay Later</h2>
                    </Card.Title>
                    <Card.Text>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam vitae nesciunt eos similique. Eum expedita dolor inventore natus dolorem blanditiis sapiente unde, temporibus facilis quos delectus eligendi eveniet, mollitia ipsa.
                    </Card.Text>
                </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className = "cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>Be Part of Our Community</h2>
                    </Card.Title>
                    <Card.Text>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam vitae nesciunt eos similique. Eum expedita dolor inventore natus dolorem blanditiis sapiente unde, temporibus facilis quos delectus eligendi eveniet, mollitia ipsa.
                    </Card.Text>
                </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}