const coursesData = [
    {
        id : "wdc001",
        name : "PHP - Laravel",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras blandit, turpis a bibendum blandit, lectus dolor mollis tellus, vitae finibus nibh ligula sed odio. Proin luctus nibh magna, in lacinia felis finibus ut. ",
        price : 45000,
        onOffer: true
    },
    {
        id : "wdc002",
        name : "Python - Django",
        description: "Pellentesque eget libero quis ex iaculis tempor. Aliquam placerat ultricies venenatis. Vestibulum ullamcorper elementum faucibus. Vestibulum neque odio, maximus sit amet pulvinar id, lobortis nec ex. Phasellus ipsum dui, maximus sit amet sapien at, pulvinar dapibus ex.",
        price : 50000,
        onOffer: true
    },
    {
        id : "wdc003",
        name : "Java - Springboot",
        description: "Nullam tristique justo at sapien pulvinar, eget mollis orci pretium. Etiam mattis lacus vel dui facilisis, quis dignissim nibh auctor. Vivamus quis justo sodales, faucibus elit sit amet, imperdiet lorem. Praesent mattis vulputate ornare.",
        price : 55000,
        onOffer: true
    }

]

export default coursesData;