import { Navigate } from 'react-router-dom';
import {useContext, useEffect} from 'react' // destructures React to useContext, useEffect, useState without dot notation
import UserContext from '../UserContext'

export default function Logout() {

    // para maaccess yung mga values ni UserProvider from App.js
    const {unsetUser, setUser} = useContext(UserContext)

    // updates localStorage to empty / clears the storage. kaso may laman parin na token sa context
    unsetUser();

    
    useEffect(() => {
        //setter function
        // sets user state saved in context to null
        setUser({
            id: null,
            isAdmin: null
        });
    })
    //localStorage.clear();

    // Redirect back to login
    return (
        <Navigate to='/login' />
    )

}