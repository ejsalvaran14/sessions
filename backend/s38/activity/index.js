let http = require("http");

let port = 4000;

let app = http.createServer((req, res) => {
    if (req.url == "/" && req.method == "GET"){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end("Welcome to Booking System");
    }else if(req.url == "/profile" && req.method == "GET"){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end("Welcome to Your Profile");
    }else if(req.url == "/courses" && req.method == "GET"){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end("Here's our courses available");
    }else if (req.url == "/addcourse" && req.method == "POST"){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end("Add a course to our resources");
    }else if(req.url == "/updatecourse" && req.method == "PUT"){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end("Update a course to our resources");
    }else if(req.url == "/archivecourse" && req.method == "DELETE"){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end("Archive courses to our resources");
    }
    


})


//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;
