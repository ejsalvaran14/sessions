const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 3001;

//MongoDB Connection using Mongoose

mongoose.connect("mongodb+srv://admin:admin123@cluster0.zkewxsi.mongodb.net/B305-to-do?retryWrites=true&w=majority", {
    useNewUrlParser : true,
    useUnifiedTopology : true
    // Allows us to avoid any current or future errors while connecting to mongoDB
});
//set notification for connection success/failure
let db = mongoose.connection; // tocheck if successful connection
//if error occured, output in console.
db.on("error", console.error.bind(console, "Connection Error!"))

// if connection is successful, output in console
db.once("open", () => console.log("We're connected to the cloud database."));

// [SECTION] Mongoose Schema
// Schemas determine the structureof the documents to be written in the database
//new mongoose.Schema is a duplicate in mongodb
const taskSchema = new mongoose.Schema({
    //define fields with corresponding data type
    name : String,
    status : {
        type : String,
        default : "pending" //default -> equips pre-defined values 
    }

})  

// [SECTION] Models (best practice all uppercase)
const Task = mongoose.model("Task", taskSchema);



//middlewares
//allows our app to read json data type
// allows to read and accept data in forms
app.use(express.json())// to read data into json format
app.use(express.urlencoded({extended : true}))// to read and accept data in forms sa body

// TASK ROUTES
// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
    - If the task already exists in the database, we return an error
    - If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res) =>{
    Task.findOne({name: req.body.name}).then((result, err) => {
        // if document was found or already existing 
        if(result != null && result.name === req.body.name){
            return res.send("Duplicate task found!")
        }else{
            let newTask = new Task({
                name : req.body.name
            })

            newTask.save().then((savedTask, saveErr) => {
                if (saveErr){
                    return console.error(saveErr);
                }else{
                    // if there is no error
                    return res.status(201).send("New  Task Created!");
                }
            })
        }
    })
})

// GET ALL TASK
app.get("/tasks", (req, res) =>{
    Task.find({}).then((result, error) => {
        // if error occured
        if(error){
            return console.error(error);
        }else{
            return res.status(200).json({
                data : result
            })
        }
    })
})





// port listening
if (require.main === module){
    app.listen(port, () => console.log(`Server is running at port ${port}`))
}

module.exports = app;