// [SECTION] Dependencies and Modules
const express = require('express');
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth.js")

//Destructure from auth
const {verify, verifyAdmin} = auth;

// Create course
router.post("/",verify, verifyAdmin, courseController.addCourse);

//Get all courses

router.get("/all", courseController.getAllCourses)

// Get all "active" course

router.get("/", courseController.getAllActive)

// Get specific course using its ID

router.get("/:courseId", courseController.getCourse)

// Updating a Course(Admin Only)

router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

// Archiving a Course(Admin Only)

router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse)

//Activating a Course (Admin Only)

router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse)

// [SECTION] Export Route System
module.exports = router;