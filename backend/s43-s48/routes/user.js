// [SECTION] Dependencies and Modules
const express = require('express');
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth.js")

//Destructure from auth
const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component


// check email routes
router.post('/checkEmail', (req, res) =>{
    userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

// user registration routes
router.post("/register", (req, res)=>{
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// user authentication
router.post('/login', userController.loginUser);

// retrieve user details
/* router.post("/details", verify, (req, res) => {
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
})  */

router.get("/details", verify, userController.getProfile);

// Enroll user to a course
router.post("/enroll", verify, userController.enroll)

// Get users enrollments
router.get("/getEnrollments", verify, userController.getEnrollments)


// [SECTION] Export Route System
module.exports = router;