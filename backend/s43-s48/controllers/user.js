// [SECTION] Dependencies and Modules
const User = require('../models/User');
const Course = require("../models/Course.js")
const bcrypt = require('bcrypt');

const auth = require('../auth');
//check email exists
/*
    Steps:
        1. Use mongoose 'find' method to find duplicate emails
        2. Use the 'then' method to send a response back to the frontend application based on the result of the 'find' method.
*/

module.exports.checkEmailExist = (reqBody) =>{
    console.log(reqBody);
    return User.find({email: reqBody.email}).then(result => {
        console.log(result);
        if(result.length >0){
            
            return true;
        }else{
            return false;
        }
    })
}

// user registration
/*
        Steps:
        1. Create a new User object using the mongoose model and the information from the request body
*/

module.exports.registerUser = (reqBody) =>{
    console.log(reqBody);
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
    });

    return newUser.save().then((user, error) =>{
        if(error){
            return false;
        }else {
            return true;
        }
    })
    .catch(err => err);
}

//user authentication
module.exports.loginUser = (req, res) =>{
    //Quick question, what do you think the first thing that we should do to check if the user is authehnticated or not? answer:check if email is existing 
    return User.findOne({email:req.body.email}).then(result => {
        console.log(result);
        if (result == null){
            return res.send(false) // the email doesn't exist in our DB
        }else{
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
            console.log(isPasswordCorrect);

            if(isPasswordCorrect){
                return res.send({access: auth.createAccessToken(result)})
            }else{
                return res.send(false); //if passwords do not match
            }
        }
    })
    .catch(err => res.send(err))

}

// Retrieve user details

module.exports.getProfile = (req, res) => {
    return User.findById(req.user.id)
      .then((result) => {
        result.password = "";
        return res.send(result);
      })
      .catch((error) => error);
  };

  module.exports.enroll = async (req, res) =>{
    console.log(req.user.id);
    console.log(req.body.courseId);

    if(req.user.isAdmin){
        return res.send("You cannot do this action.")
    }
    let isUserUpdated = await User.findById(req.user.id)
        .then(user =>{
            let newEnrollment = {
                courseId: req.body.courseId,
                courseName: req.body.courseName,
                courseDescription: req.body.courseDescription,
                coursePrice: req.body.coursePrice
            }

            user.enrollments.push(newEnrollment);

            return user.save().then(user => true).catch(error => res.send(error));
        })

        if(isUserUpdated !== true){
            return res.send({message: isUserUpdated})
        }
        let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
            let enrollee = {
                userId: req.user.id
            }
            
            course.enrollees.push(enrollee);
            
            return course.save().then(course => true).catch(error => res.send(error))
        })

        if(isCourseUpdated !== true){
            return res.send({message: isCourseUpdated});
        }

        if(isUserUpdated && isCourseUpdated){
            return res.send("You are now enrolled to a course")
        }
  }

  module.exports.getEnrollments = (req, res) => {
    return User.findById(req.user.id).then((result) => {
      if (result.isAdmin) {
        return res.send(`You cannot do this action`);
      }
  
      if (result.enrollments.length < 1) {
        return res.send(`No Courses Enrolled`);
      } else {
        return res.send(result.enrollments);
      }
    });
  };