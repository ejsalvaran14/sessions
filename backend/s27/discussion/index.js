//Array Mutator Method

let fruits = ["apple", "orange", "kiwi", "Dragon Fruit"];

//push();
//add elements at the end of an array

console.log("Current Array: ");
console.log(fruits);
let fruitsLength = fruits.push("Mango");
console.log("Array after pushed method ");
console.log(fruitsLength);
console.log(fruits);

// Push multiple elements
fruits.push("avocado", "guava");
console.log("Array after pushed method ");
console.log(fruits);


function addFruit(fruit){
    // push parameter
    fruits.push(fruit);
    console.log(fruits);
}

addFruit("Pineapple");

//pop()
//Removes elemt at the end of an array

fruits.pop();
fruits.pop();
console.log(fruits)

function removeFruit(){
    fruits.pop();
    console.log(fruits);
}

removeFruit();

// unshift()
// add element in the beginning of an array
fruits.unshift("Lime", "Banana");
console.log("Array after unshift method: ");
console.log(fruits);

function unshiftFruit(fruit){
    fruits.unshift(fruit);
    console.log(fruits);
}

unshiftFruit("Calamansi");

//shift()
// remove element in the beginning of an array

fruits.shift();
console.log("Array after shift method: ");
console.log(fruits);

function shiftFruit(){
    fruits.shift();
    console.log(fruits);
}

shiftFruit();

//splice()

//simultaneously removes elements from a spliced index number and adds element
//syntax: array.splice(startIndex, deleteCount, elementsToBeAdded);

fruits.splice(1, 2, "Avocado");
console.log("array after splice method");
console.log(fruits);

function spliceFruit(index,deleteCount,fruit){
    fruits.splice(index,deleteCount,fruit);
    console.log(fruits);
}

spliceFruit(1, 1, "Cherry");
spliceFruit(2, 1, "Durian");

//sort()
// arranges the elements in alphanumeric order

fruits.sort();
console.log("Array after sort method");
console.log(fruits);

//reverse()

fruits.reverse();
console.log("Array reverse sort method");
console.log(fruits);