// console.log("Hello World");


//Objective 1
//Add code here
//Note: function name is numberLooper

function numberLooper(num1){
    console.log("The number your provided is " + num1);
    for(num1;num1>=50;num1--){

        if(num1 % 5 === 0 && num1 % 10 !== 0){
            console.log(num1)
        }
        if(num1 % 10 ===0){
            console.log("The number is divisible by 10, skipping the number.")
        }
    }
};


numberLooper(95);





//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here


function removeVowels(string) {

    const vowels = "aeiouAEIOU";
    
  
    for (let i = 0; i < string.length; i++) {
      const currentChar = string[i];
  //this if statment tells that if the current char is not within the vowels string, it will be printed
      if (vowels.indexOf(currentChar) === -1) {
        filteredString += currentChar;
      }
    }
  
    return filteredString;
  }
  
  filteredString = removeVowels(string);
  console.log(filteredString);





//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null,
        numberLooper: typeof numberLooper !== 'undefined' ? numberLooper : null
    }
} catch(err){

}