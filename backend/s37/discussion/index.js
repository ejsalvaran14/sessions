//node.js
// client-server architecture

//client applications that consists our browser
// db servers -> systems developed using node
//Database ->MongoDB

//Benefits
// 1. Perforamnce -> optimized for web applications

// 2. Familiarity -> same old JS

// NPM - Node Package Manager

//Use the "require" directive to load Node.js
//"http module" contains the components needed to create a server.
let http = require("http");

// "createServer()" listens to a request from the client.
// A port is a virtual point where network connection start and end.
// ".listen()" where the server will listen to any request sent in our server
http.createServer(function(request, response){

    //"writeHead()" sets the status code for response
    //status code 200 means ok
    //refer to https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
    response.writeHead(200,{"Content-Type" : "text/plain"});

    //Send teh response with text content "Hello World!"
    response.end("Hello World!");

}).listen(4000);

// Output will be shown in the terminal
console.log("Server is running at localhost: 4000");
