//[ SECTION ] -if, else if, else statement

let numA = -1;
//if statement
/*
Syntax:
if (condition){
    //code
}
*/
if (numA < 5){
    console.log("Hello")
};

//false condition will not execute the code
if (numA < -5){
    console.log("Hello")
};

//string

let city = "New York";
if (city === "New York"){

    console.log("Welcome to New York City")
}

//else if

let numH = 1;

if(numH < 0){
    console.log("Hello!")
}else if(numH > 0){
    console.log("World!")
}

city = "Tokyo";

if(city === "New YOrk"){
    console.log("Welcome to New York City")
}else if (city === "Tokyo"){
    console.log("Welcome to Tokyo Japan")
}

//else statement

let num1 = 5;

if(num1 === 0){
    console.log("hello")
}else if(num1 ===3) {
    console.log("world")
}else{
    console.log("Sorry, all conditions are not met")
}

//if, else if, else with functions

function determineTyphoonIntensity(windSpeed){
    if(windSpeed < 30){
        return "Not a Typhoon yet"
    }else if(windSpeed<= 61){
        return "Tropical depression detected"
    }else if(windSpeed >= 62 && windSpeed <= 88){
        return "Tropical Storm detected."
    }else if (windSpeed>=89 && windSpeed <=117){
        return "severe tropical storm detected"
    }else{
        return "Typhoon detected."
    }
} 
let message = determineTyphoonIntensity(25)
message = determineTyphoonIntensity(85);
message = determineTyphoonIntensity(250);
console.log(message);

//Truthy

if(true){
    console.log("Truthy")
};

if(1){
    console.log("Truthy")
};

if([]){
    console.log("Truthy")
};

//Single statement execution
//Ternary Statement

let num11 = 10;

let ternary = (num11 < 18 || num11 !== "10") ? "Yes" : "No"
console.log(ternary);

//multiple statements

let name;

function isOfLegalAge(){
    name = "John";
    return "You are of legal age limit."
}

function isUnderAge(){
    name = "Jane"
    return "You are under age"
}

let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name);

//switch-case statement 
let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day){
    case "monday" : 
        console.log("The color of the day is red.");
        break;
    case "tuesday":
        console.log("The color of the day is orange.");
        break;
    case "wednesday":
        console.log("The color of the day is yellow.");
        break;
    case "thursday":
        console.log("The color of the day is green.");
        break;
    case "friday":
        console.log("The color of the day is blue.");
        break;
    case "saturday":
        console.log("The color of the day is indigo.");
        break;
    case "sunday":
        console.log("The color of the day is violet.");
        break;
    default: 
    console.log("Please enter a valid day")
}