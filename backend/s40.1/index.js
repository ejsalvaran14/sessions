const express = require("express");

const app = express();
const port = 4000;

// Middlewares
app.use(express.json());
//Allows your add to read data from forms
app.use(express.urlencoded({extended:true}))
//accepts all data types

// [SECTION] Routes
// GET method
app.get ("/", (req,res) => {
    res.send("Hello World");
})

app.get("/hello", (req,res) => {
    res.send("Hello from /hello end point.");
})

// route expects to receive a POST method at the URI "/hello"
app.post("/hello", (req, res) => {
    res.send(`Hello there, ${req.body.firstName} ${req.body.lastName}!`);
})

// POST route to register user
let users = [];

app.post("/signup", (req, res) => {
    console.log(req.body);
    if (req.body.username !== "" && req.body.password !== ""){
        users.push(req.body);
        res.send(`User ${req.body.username} successfully registered!`)
    } else{
        res.send("Please input both username and password");
    }
})

//change password PUT method
app.put("/change-password", (req, res) => {
    let message;
    for (let i = 0; i < users.length; i++){
        if(req.body.username === users[i].username){
            users[i].password = req.body.password

            message = `User ${req.body.username}'s password has been updated`
            break;
        } else{
            message = "user does not exist.";
        }
    }
    res.send(message);
})

if(require.main === module){
    app.listen(port, () => console.log(`server is running at ${port}`));
}