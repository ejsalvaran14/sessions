// CRUD Operations (MongoDB)
// CRUD Operatinos are the heart of any backend app.
// Mastering CRUD Operations is essential for any developers

/*
C-Create/Insert
R-Retrieve/Read
U-Update
D-Delete
*/

// [SECTION] Creating documents

/*
SYNTAX:
db.users.insertOne({object})

SQL:
INSERT INTO table_name(col1,...)
VALUES(value1,...);
 */

db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "09123456789",
        email: "janedoe@gmail.com"
    },
    courses: ["CSS", "JS", "Python"],
    department: "none"
});

db.users.insertOne({
    firstName: "Earl Jerin",
    lastName: "Alvaran",
    age: 21,
    contact: {
        phone: "09123456789",
        email: "ejsalvaran14@gmail.com"
    },
    courses: ["CSS", "JS", "Python"],
    department: "anti-silos"
});

/* 

SYNTAX:
db.users.insertMany([{object}])
*/

db.users.insertMany([
    {
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: {
        phone: "09123456789",
        email: "stephenhawking@gmail.com"
    },
    courses: ["PHP", "React", "Python"],
    department: "none"
},

    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 21,
        contact: {
            phone: "09123456789",
            email: "neilarmostrong@gmail.com"
        },
        courses: ["React", "Laravel", "Sass"],
        department: "none"
    }


]);

// [SECTION] -Read/Retrieve

/*
SYNTAX:

db.users.findOne();
db.users.findOne({field: value});

*/

db.users.findOne();

db.users.findOne({firstName: "Stephen"});






/*
SYNTAX:
db.users.find();
db.users.find({field: value});
*/

db.users.find({department:"none"});

// multiple criteria

db.users.find({department:"none", age: 82});



db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "0000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});

// [SECTION] Updating a data

/* 
SYNTAX:
db.collectionName.updateOne({criteria}, {$set: {field: value}});
*/

db.users.updateOne(
    {firstName: "Test"},
    {$set: {
        firstName: "Bill", 
        lastName: "Gates",
        age: 65,
        contact:{
            phone: "123456789",
            email: "billgates@gmail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department:"Operations",
        status: "active"
    }}
);

//find the data
db.users.findOne({firstName: "Bill"});
db.users.findOne({firstName: "Test"});

db.users.findOne({"contact.email": "billgates@gmail.com"});


// Update many collection

/*
db.users.updateMany({criteria}, {$set: {field: value}});
*/

db.users.updateMany(
    {department: "none"},
    {
        $set: {
            department: "HR"
        }
    }
);

//[SECTION] Deleting a data

db.users.insert({
    firstName: "Test"
});

// Delete single document

/*
SYNTAX:
db.users.deleteOne({criteria})
*/

db.users.deleteOne({
    firstName: "Test"
});

// Delete Many
/*
db.users.deleteMnay({criteria})
*/

// Inserting another bill
db.users.insert({
    firstName: "Bill"
});

db.users.deleteMany({firstName: "Bill"});


