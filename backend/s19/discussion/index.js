console.log("hello world");
// [SECTION] Syntax, Statements and Comments
// JS Statements usually end with semicolon(;)

// COmments:

// THere are two types of comments:
// 1. The single-line comment denoted by //, double slash
// 2. The multiple-line comment denoted by /* */,
// double and asterisk in the middle


// [SECTION] Variables
// It is used to contain data
// Any information that is used by an application is stored in what we call a "memory"

// Declaring Variables

// Declaring Variables - tells our devices that a variable name is created.
 let myVariableNumber; //  camel casing naming convension 
 
 console.log("myVariableNumber");
 console.log(myVariableNumber);

 // Syntax
    // let/const/var variableName

// Declaring and Initializing Variables

let productName = "desktop computer";
console.log(productName);

let productPrice1 = "18999";
let productPrice = 18999;
console.log(productPrice1);
console.log(productPrice);

// const= constant meaning impossible to reassign unlike let
const interest = 3.539;
// Reassigning names

productName = "laptop";
console.log(productName);

// interest= 4.239;
// console.log(interest);
// will return an error 

// Declare the variable

myVariableNumber =2023;
console.log(myVariableNumber);

// multiple variable declarations

let productCode = 'DC017'
const productBrand  = 'Dell'

console.log(productCode, productBrand);

// [SECTION] DATA TYPES
// STRINGS
 let country = 'Phillipines';
 let province = 'Metro Manila';

 // Concatenation Strings
 let fullAddress = province + "," + country;
 console.log(fullAddress);

 let greeting = "I live in the " + country;
 console.log(greeting);

 let mailAddress = 'Metro Manila \n\n Philippines';
 console.log(mailAddress);

 let message = 'John\'s employees went home early again!';
 console.log(message);

 //Numbers
 let headcount  = 26;
 console.log(headcount);
 //Decimal
 let grade = 98.7;
 console.log(grade);

//  Exponential Notations
let planetDistance = 2e10;
console.log(planetDistance);

// combining strings and texts
console.log("John's grade last quarter is " + grade);

// BOolean
// Returns true or false

let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays
let grades = [95, 49, 29, 39, 51];
console.log(grades);

console.log(grades[0]);
console.log(grades[1]);
console.log(grades[2]);
console.log(grades[3]);
console.log(grades[4]);
console.log(grades[5]);

let details = ["john", "smith", 32, true];
console.log(details)

let person = {
    fullName: "Juan Dela Cruz",
    age: 35,
    isMarried: false,
    contact: ["09123455677","099851532"],
    address: {
        houseNumber: "345",
        city: "Manila"
    }
};
console.log(person);
console.log(person.fullName);
console.log(person.age);
console.log(person.isMarried);
console.log(person.contact[0]);
console.log(person.contact[1]);
console.log(person.address);
console.log(person.address.houseNumber);
console.log(person.address.city);
