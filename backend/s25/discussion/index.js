// JS Objects

/*
syntax:

keyA: valueA,
keyB: valueB
*/

let cellphone={
    name: "Nokia 3210",
    manufacutreDate: 1999
}

console.log("Result from creating:");
console.log(cellphone);
console.log(typeof cellphone);

//Creating object using a constructor function
/*
function objectName(KeyA, keyB){
    this.keyA = keyA,
    this.keyB = keyB
}
 */

//This is an Object
// "this" keyword assigning value to our property

function Laptop(brand, name, manufactureDate){
    this.brand = brand,
    this.name = name,
    this.manufactureDate = manufactureDate
}

//Instance -> "new" because we want to create an object using our laptop blueprint
// Instance -> duplicate -> make a copy of object

let laptop = new Laptop("Acer", "Predator 300", "2022");
console.log("Result from instance:");
console.log(laptop);

let myLaptop = new Laptop("Apple", "Macbook Air", "2022");

console.log(laptop);


let car = {};
//dot notation

car.name = "Honda Civic";
console.log("Result of adding properties: ");
console.log(car);

//bracket notation
car["manufactureDate"] = 2019;
console.log(car["manufactureDate"]);
console.log(car.manufactureDate);
console.log(car);

//Object Methods

let person = {
    name: "John",
    talk: function(){
        console.log("ello my name is " + this.name)
    }
}

console.log(person);
console.log("Result from object methods: ");
person.talk();

// Add methos to object
person.walk = function(){
    console.log(this.name + " walked 25 steps forward");
}

person.walk();

let friend = {
    firstName: "Joe",
    lastName: "Smith",
    address: {
        city: "Austin",
        country: "Texas"
    },
    emails: ["joe@mail.com", "joesmith@mail.com"],
    introduce: function(){
        console.log("Hello my name is "+ this.firstName + " " + this.lastName);
    }
}

friend.introduce();
console.log(friend.emails[1]);