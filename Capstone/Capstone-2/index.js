// [SECTION ]Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

require('dotenv').config();

const userRoutes = require("./routes/userroutes");// allows access to routes defined within our application
const productRoutes = require("./routes/productroutes");
const cartRoutes = require("./routes/cartroutes");
// [SECTION] Environment Setup
const port = 4000;

// [SECTION] Server Setup
const app = express();

//middlewares
app.use(express.json());// this project will assume json format
app.use(express.urlencoded({extended:true})); // way to parse incoming request body
app.use(cors()); //Allows all resources to access our backend application


// [SECTION] Database Connection
mongoose.connect("mongodb+srv://admin:admin123@cluster0.zkewxsi.mongodb.net/Capstone-2?retryWrites=true&w=majority", 
    {
    useNewUrlParser : true,
    useUnifiedTopology : true
    }
);

mongoose.connection.on("error", console.error.bind(console, "Connection Error!"));
mongoose.connection.once('open',()=> console.log('Now connected to MongoDB Atlas!'));

// [SECTION] Backend Routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/carts', cartRoutes);

// [SECTION] Server Gateway Response
if(require.main === module){
    app.listen(process.env.PORT || port, ()=> {
        console.log(`API is now online on port ${process.env.PORT || port}`)
    });
};

module.exports = app;
