// [SECTION] Dependencies and Modules
const express = require('express');
const router = express.Router();
const cartController = require("../controllers/cartcontrollers.js");
const auth = require("../auth.js")

//Destructure from auth
const {verify, verifyAdmin} = auth;

//Adding to cart
router.post("/addCart", verify, cartController.addCart)

//Retrieve authenticated user's orders
router.get("/cartSummary", verify, cartController.cartSummary)

//Retrieve all orders (Admin Only)
router.get("/allOrders", verify, verifyAdmin, cartController.allOrders)

router.delete("/removeProduct", verify, cartController.deleteOneItem)

//Update quantity amounts
router.put("/updateQuantity", verify, cartController.updateCart)

router.post("/checkout", verify, cartController.checkout)
// [SECTION] Export Route System
module.exports = router;
