// [SECTION] Dependencies and Modules
const express = require('express');
const router = express.Router();
const userController = require("../controllers/usercontrollers.js");
const auth = require("../auth.js")

//Destructure from auth
const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component

// Registering user
router.post("/register", userController.registerUser);

// user authentication
router.post('/login', userController.loginUser);

// Create Order
router.post("/order", verify, userController.order)

router.get("/details",verify,  userController.getProfile)

//[SECTION] Reset Password
router.put('/reset-password', verify, userController.resetPassword);

//[SECTION] Update Profile  
router.put('/profile', verify, userController.updateProfile);




// [SECTION] Export Route System
module.exports = router;
