// [SECTION] Dependencies and Modules
const express = require('express');
const router = express.Router();
const productController = require("../controllers/productcontrollers.js");
const auth = require("../auth.js")

//Destructure from auth
const {verify, verifyAdmin} = auth;

//Create a Product (ADMIN only)
router.post("/addProduct",verify, verifyAdmin, productController.addProduct);

//Retrieve all products
router.get("/all", productController.getAllProducts)

//Retrieve all active products
router.get("/", productController.getAllActive)

//Retrieve a single product
router.get("/:productId", productController.getProduct)

// Update Product Information (ADMIN only)
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

//Archive a Product(ADMIN only)
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct)

//Activate a Product(ADMIN only)
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct)

//Product searching by name
router.post("/searchProduct", productController.productSearch)














// [SECTION] Export Route System
module.exports = router;