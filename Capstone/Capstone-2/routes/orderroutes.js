// [SECTION] Dependencies and Modules
const express = require('express');
const router = express.Router();
const orderController = require("../controllers/ordercontrollers.js");
const auth = require("../auth.js")

//Destructure from auth
const {verify, verifyAdmin} = auth;

router.get(`/`, verify, orderController.getUserOrders);

// [SECTION] Export Route System
module.exports = router;
