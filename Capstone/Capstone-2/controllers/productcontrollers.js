// [SECTION] Dependencies and Modules
const Product = require('../models/Product');
const bcrypt = require('bcrypt');

const auth = require('../auth');

// Product Registration
module.exports.addProduct = (req, res) =>{
    
    let newProduct = new Product({
        productName : req.body.productName,
        productDescription : req.body.productDescription,
        price : req.body.price,
        
    });

    return newProduct.save().then((product, err) =>{
        if(err){
           return res.send(false);
        }else {
           return res.send(true);
        }
    })
    .catch(err => err);
}

//Retrieve all Products
module.exports.getAllProducts = (req, res) =>{
    //.select('productName productDescription price')
    return Product.find({}).then(result=>{
        if(result.length === -1){
            return res.send(false);
        }else{
            return res.send(result);
        }
    })
}

//Retrieve all active Products
module.exports.getAllActive = (req, res)=>{
    return Product.find({isActive: true}).select('productName description price').then(result => {
        if (result.length === 0){
            return res.send(false);
        }else {
            return res.send(result);
        }
    })
}

//Retrieve a single product
module.exports.getProduct = (req, res) =>{
    return Product.findById(req.params.productId).select('productName description price')
    .then(result =>{
        if (result === 0){
            return res.send("Cannot find product with the ID provided");
        }else{
            if(result.isActive === false){
                return res.send("the product is not available");
            }else{
                return res.send(result);
            }
        }
    })
    .catch(error => res.send("Please enter a correct product ID"));
}

//Update a product information (Admin Only)
module.exports.updateProduct = (req, res) =>{
    let updatedProduct = {
        productName: req.body.productName,
        productDescription: req.body.productDescription,
        price: req.body.price
    }
    return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product,error) =>{
        if(error){
            return res.send(false);
        }else{
            //return res.send(true);
            console.log(product);
             return res.send(product);
        }
    })
}

//Archive a Product (Admin Only)
module.exports.archiveProduct = (req, res) =>{
    let archivedProduct = {
        isActive : false
    }
    return Product.findByIdAndUpdate(req.params.productId, archivedProduct).then((product,error) =>{
        if(error){
            return res.send(false);
        }else{
            console.log("Successfully archived a Product!")
            return res.send(true);
        }
    })
}

//Activate a Product (Admin Only)
module.exports.activateProduct = (req, res) =>{
    let activatedProduct = {
        isActive : true
    }
    return Product.findByIdAndUpdate(req.params.productId, activatedProduct).then((product,error) =>{
        if(error){
            return res.send(false);
        }else{
            console.log("Successfully activated a Product!")
            return res.send(true);
        }
    })
}

module.exports.productSearch = async (req, res) =>{

    try {
        const searchProduct = await Product.find({
          $or: [
            { productName: { $regex: new RegExp(req.body.productName, "i") } },
            //{ category: { $regex: new RegExp(req.body.productName, "i") } },
          ],
        }).sort({ productName: -1 }); // Sort by productName in descending order
    
        if (searchProduct.length > 0) {
          console.log(`${searchProduct}`);
          return res.send(searchProduct); // Products exist
        } else {
          console.log("Product doesn't exist");
          return res.send([]); // Product doesn't exist
        }
      } catch (error) {
        console.error("Error:", error);
        return res.status(500).json({ error: "Internal Server Error" });
      }
}