// [SECTION] Dependencies and Modules
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const Cart = require('../models/Cart');
const User = require('../models/User');
const auth = require('../auth');
const Order = require('../models/Order')


module.exports.addCart = async (req, res) => {
  try {
    // const { productName, quantity } = req.body;
    const productName = req.body.productName;
    const quantity = Number(req.body.quantity);

    // Find the product by its name (assuming productName is unique)
    const product = await Product.findOne({ productName });

    if (!product) {
      return res.status(404).json({ message: "Product does not exist" });
    }

    const { _id, productDescription, price } = product;
    const totalPrice = price * quantity;

    // Find the user's cart
    let userCart = await Cart.findOne({ userId: req.user.id });

    if (!userCart) {
      // If the user doesn't have a cart, create a new one
      userCart = new Cart({
        userId: req.user.id,
        products: [{
          productId: _id,
          productName,
          productDescription,
          quantity,
          price,
          totalPrice,
        }],
        totalAmount: 0,
      });
    }

    // Check if the product is already in the cart
    const existingProduct = userCart.products.find(
      (cartProduct) => cartProduct.productId.toString() === _id.toString()
    );

    if (existingProduct) {
      // Update the quantity, price, and totalPrice of the existing product
      existingProduct.quantity += quantity;
      existingProduct.price = price;
      existingProduct.totalPrice += totalPrice;
    } else {
      // Add the product as a new item in the cart
      userCart.products.push({
        productId: _id,
        productName,
        productDescription,
        quantity,
        price,
        totalPrice,
      });
    }

    // Calculate the new totalAmount for the cart
    userCart.totalAmount += totalPrice;

    // Save the updated or new cart to the database
    await userCart.save();

    // Return detailed information about the added/updated product
    res.status(201).json({
      message: "Added to cart successfully",
      cart: {
        productName,
        productDescription,
        quantity,
        price,
        totalPrice,
      },
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

//Retrieve authenticated user's orders


module.exports.cartSummary = async (req, res) =>{
 try{
  const user = await User.findById(req.user.id)
  if (!user){
    console.log(`No logged in user`)
    return res.send(false)
  }
  const cart = await Cart.findOne({userId : req.user.id})
  if (!cart){
    console.log("There are no active carts in the DB")
    return res.send(null);
  }
  
  const viewCart = {
    ...cart.toObject()//works like for each(for viewing only)
  }
  console.log(viewCart)
  return res.send(viewCart)
 }catch(err){console.log(err)}
  
}

module.exports.allOrders = async (req, res) =>{
  try{
    return Cart.find({}).then(result=>{
      if(result.length === 0){
          return res.send("There are no carts in the DB.");
      }else{
          return res.send(result);
      }
  })

  }catch(err){console.log(err)}
  
}

//deleting cart item
module.exports.deleteOneItem = async (req, res) => {
  try {
    // Get the user's cart
    const cart = await Cart.findOne({ userId: req.user.id });

    if (!cart) {
      console.log(`Cart not found`)
      return res.send(null);
    }

    const productNameToDelete = req.body.productName;

    // Find the index of the product in the cart's items array based on the product name (case-insensitive)
    const productIndexToDelete = cart.products.findIndex((item) =>
      new RegExp(`^${productNameToDelete}$`, "i").test(item.productName)
    );

    if (productIndexToDelete === -1) {
      console.log(`Product not found in the cart`);
      return res.send(false);
    }

    // Remove the entire product from the cart
    cart.products.splice(productIndexToDelete, 1);

    // Save the updated cart
    await cart.save();

    console.log(`Product removed from the cart`);
    return res.send(true);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: `Internal Server Error` });
  }
};

//updating cart amounts
module.exports.updateCart = async (req, res) => {
  try {
    const userId = req.user.id;
    const productNameToUpdate = req.body.productName;
    const newQuantity = Number(req.body.quantity);

    // Find the cart for the user
    const cart = await Cart.findOne({ userId });

    if (!cart) {
      console.log(`Cart not found`);
      return res.status(404).send(false);
    }

    // Find the index of the product in the cart's items array based on the product name (case-insensitive)
    const productIndex = cart.products.findIndex((item) =>
      new RegExp(`^${productNameToUpdate}$`, "i").test(item.productName)
    );

    if (productIndex === -1) {
      console.log(`Product not found in the cart`);
      console.log(productIndex);
      return res.status(404).send(false);
    }

    // Update the quantity of the product
    cart.products[productIndex].quantity = newQuantity;

    // Save the updated cart
    await cart.save();

    console.log(`Product updated!`);
    return res.send(true);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: `Internal Server Error` });
  }
};


// Check out cart and push it to the Orders
 module.exports.checkout = async (req, res) => {
  try {
    // Get the user's cart
    const cart = await Cart.findOne({ userId: req.user.id });

    if (!cart) {
      console.log(`Cart is Empty`);
      return res.send(null);
    }

    // Calculate the total cart amount and totalCartPrice
    let totalCartAmount = 0; // Initialize totalCartAmount to zero
    let totalCartPrice = 0; // Initialize totalCartPrice to zero

    // Update product stocks and create an array to track updated products
    const myProducts = [];

    for (const requestedItem of req.body.productsToCheckout) {
      // Case-insensitive search for the product within the cart's items
      const item = cart.products.find((cartItem) =>
        new RegExp(`^${requestedItem.productName}$`, "i").test(
          cartItem.productName
        )
      );

      if (!item) {
        console.log(`product not found in cart`);
        return res.send(
          `Product not found in cart: ${requestedItem.productName}`
        );
      }

      // Find the product from the database using a case-insensitive query
      const product = await Product.findOne({
        productName: new RegExp(`^${requestedItem.productName}$`, "i"),
      });

      // if (!product) {
      //   return res.send(`Product not found: ${requestedItem.productName}`);
      // }

      if (requestedItem.quantity > item.quantity) {
        return res.send(
          `Requested quantity exceeds cart quantity for ${requestedItem.productName}`
        );
      }

      if (requestedItem.quantity > product.stock) {
        return res.send(`Insufficient stock for ${requestedItem.productName}`);
      }

      // Update product stock
      product.stock -= requestedItem.quantity;
      await product.save();

      // Deduct the quantity from the cart
      item.quantity -= requestedItem.quantity;

      // Calculate the total price for the current item
      const itemPrice = product.price * requestedItem.quantity;

      // Add the updated product to the array with quantity
      myProducts.push({
        productId: product._id,
        productName: product.productName,
        updatedQuantity: requestedItem.quantity,
        price: itemPrice, // Add the price to the myProducts array
        quantity: requestedItem.quantity, // Add the quantity to the myProducts array
      });

      // Update the total cart amount and totalCartPrice
      totalCartAmount += itemPrice;
      totalCartPrice += itemPrice;
    }

    // Save the billing address to the user's details
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.send(`User not found`);
    }

    user.billingAddress = req.body.billingAddress; // Update the billingAddress field

    await user.save();

    // Clear the user's cart of items with quantity <= 0
    cart.products = cart.products.filter((item) => item.quantity > 0);

    // Check if the cart is empty and delete it if it is
    if (cart.products.length === 0) {
      await Cart.findOneAndDelete({ userId: req.user.id });
    } else {
      // Save the updated cart
      await cart.save();
    }

    // Create an order object for the current checkout
    const order = {
      userId: req.user.id,
      //userName: user.fullName,
      products: myProducts, // The array of updated products from the cart
      purchasedOn: new Date(),
      totalCartPrice: totalCartPrice, // Add totalCartPrice to the order
    };
    await Order.findByIdAndUpdate(
      req.user.id, // Adjust this query to match your use case
      { $push: { activeOrders: order } },
      { new: true, upsert: true }
    );

    // Return the user's name, updated products, and total cart amount
    return res.json({
      //userName: user.fullName,
      myProducts,
      totalCartAmount,
      message: `Checkout successful`,
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: `Internal Server Error` });
  }
}; 

