// [SECTION] Dependencies and Modules
const User = require('../models/User');
const bcrypt = require('bcrypt');
const Product = require("../models/Product.js")
const auth = require('../auth');

// user registration
module.exports.registerUser = (req, res) =>{
    
    let newUser = new User({
        firstName: req.body.firstName,
		lastName: req.body.lastName,       
        email : req.body.email,
        mobileNo: req.body.mobileNo,
        password: bcrypt.hashSync(req.body.password, 10)
    });

    return newUser.save().then((user, err) =>{
        if(err){
            return res.send(false);
        }else {
            return res.send(true);
        }
    })
    .catch(err => err);
};

// User Authentication
module.exports.loginUser = (req, res) =>{
    return User.findOne({email:req.body.email}).then(result => {
        console.log(result);
        if (result == null){
            return false //res.send("The Email doesn't exist in our DB") // the email doesn't exist in our DB
        }else{
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
            console.log(isPasswordCorrect);

            if(isPasswordCorrect){
                return res.send({access: auth.createAccessToken(result)})
            }else{
                return res.send(false); //if passwords do not match
            }
        }
    })
    .catch(err => res.send(err))
}

//Create Order
module.exports.order = async (req, res) =>{
    console.log(req.user.id);
    console.log(req.body.productId);

    if(req.user.isAdmin){
        return res.send("You cannot do this action.")
    }
    let isUserUpdated = await User.findById(req.user.id)
        .then(user =>{
            let newOrder = {
                productId: req.body.productId,
                productName: req.body.productName,
                productDescription: req.body.productDescription,
                productPrice: req.body.productPrice
            }

            user.orderedProducts.push(newOrder);

            return user.save().then(user => true).catch(error => res.send(error));
        })

        if(isUserUpdated !== true){
            return res.send({message: isUserUpdated})
        }
        let isProductUpdated = await Product.findById(req.body.productId).then(product => {
            let userOrder = {
                userId: req.user.id
            }
            
            product.userOrders.push(userOrder);
            
            return product.save().then(product => true).catch(error => res.send(error))
        })

        if(isProductUpdated !== true){
            return res.send({message: isProductUpdated});
        }

        if(isUserUpdated && isProductUpdated){
            return res.send("You have successfully created an order!")
        }
  }

// Retrieve User Details
/* module.exports.getProfile = (req, res) => {
return User.findById(req.user.id)
    .then((result) => {
    result.password = "";
    return res.send(result);
    })
    .catch((error) => error);
}; */

module.exports.getProfile= (req, res) =>{
    return User.findById(req.user.id).then(result=>{
        if(result.length === 0){
            return res.send("There are existing users  in the DB.");
        }else{
            return res.send(result);
        }
    })
}

//[SECTION] Reset Password

module.exports.resetPassword = async (req, res) => {
    try {

    //console.log(req.user)
    //console.log(req.body)

      const { newPassword } = req.body;
      const { id } = req.user; // Extracting user ID from the authorization header
  
      // Hashing the new password
      const hashedPassword = await bcrypt.hash(newPassword, 10);
  
      // Updating the user's password in the database
      await User.findByIdAndUpdate(id, { password: hashedPassword });
  
      // Sending a success response
      res.status(200).send({ message: 'Password reset successfully' });
    } catch (error) {
      console.error(error);
      res.status(500).send({ message: 'Internal server error' });
    }
};


//[SECTION] Reset Profile
module.exports.updateProfile = async (req, res) => {
    try {

        console.log(req.user);
        console.log(req.body);
        
    // Get the user ID from the authenticated token
      const userId = req.user.id;
  
      // Retrieve the updated profile information from the request body
      const { firstName, lastName, mobileNo } = req.body;
  
      // Update the user's profile in the database
      const updatedUser = await User.findByIdAndUpdate(
        userId,
        { firstName, lastName, mobileNo },
        { new: true }
      );
  
      res.send(updatedUser);
    } catch (error) {
      console.error(error);
      res.status(500).send({ message: 'Failed to update profile' });
    }
  }