// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

//[SECTION] Schema/BluePrint
const productSchema = new mongoose.Schema({
    productName : {
        type: String,
        required: [true, 'Product name is required!' ] //makes all schema to require name
    },
    productDescription : {
        type: String,
        required : [true, 'Product description is required!']
    },
    price : {
        type: Number,
        required: [true, 'Product price is required!']
    },
    quantity: {
        type: Number,
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn : {
        type: Date,
        // The "new Date()" expression that instantiates a new "date" that store the current time and date whenever a course is created in our database
        default: new Date()
    }
});

// [SECTION] Model
module.exports = mongoose.model('Product', productSchema);