// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');


const cartSchema = new mongoose.Schema({
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User', // Reference to the User model for association
      required: true,
    },
    products: [
      {
        productId: {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Product',
          required: true,
        },
        productName: {
          type: String,
          required: true,
        },
        productDescription: {
          type: String,
          required: true,
        },
        quantity: {
          type: Number,
          default: 1,
          required: true,
        },
        price: {
          type: Number,
          required: true,
        },
        totalPrice: {
          type: Number,
          required: true,
        }
      },
    ],
    totalAmount: {
      type: Number,
    },
    purchasedOn: {
      type: Date
    },
  });
  
  // Calculate the totalAmount before saving
  /* cartSchema.pre('save', function (next) {
    const productsTotal = this.products.reduce((total, product) => {
      return total + product.quantity * product.price;
    }, 0);
    this.totalAmount = productsTotal;
    next();
  }); */

  // Function to calculate the total price for each item
cartSchema.methods.calculateTotalPrice = function () {
  this.products.forEach((item) => {
    item.totalPrice = item.quantity * item.price;
  });
};

// Middleware to calculate the total cart price for items before saving
cartSchema.pre("save", function (next) {
  this.calculateTotalPrice();
  this.totalAmount = this.products.reduce(
    (total, item) => total + item.totalPrice,
    0
  );
  next();
});


    module.exports = mongoose.model("Cart", cartSchema);