import React, { useState, useEffect } from "react";
import Swal from 'sweetalert2'
import CartView from '../components/CartView'

export default function Cart() {
  const [cartItems, setCartItems] = useState([]);
  //const [userName, setUserName] = useState("");


  const fetchData = () =>{
    fetch("https://capstone2-alvaran.onrender.com/carts/cartSummary", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }).then((res) => res.json())
    .then((data) =>{
      if(data) {
        setCartItems(data.products);
      }else {
        setCartItems(data.products) 
      } 
    }).catch(err =>(console.error))
  }
 

  useEffect(() => {
   fetchData();
    
  }, []); // Empty dependency array to run the effect once when the component mounts

  //For updating item quantity 
  const updateQuantity = (productName, newQuantity) => {
    const token = localStorage.getItem("token");

    if (!token) {
      // Handle the case where the user is not authenticated
      console.log("User is not authenticated.");
      return;
    }

    const requestBody = {
      productName: productName,
      quantity: newQuantity,
    };

    fetch(`https://capstone2-alvaran.onrender.com/carts/updateQuantity`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(requestBody),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          // Handle the case where the server returned an error
          console.log("Failed to update the cart.");
          return null;
        }
      })
      .then((data) => {
        if (data) {
          // If the update is successful, you can handle the updated cart data as needed
          console.log("Cart updated successfully:", data);
          // Perform any client-side updates here
          fetchData();
        }
      })
      .catch((error) => {
        // Handle any network or other errors here
        console.error("Error updating the cart:", error);
      });
  };


  //For Removing selected items in the cart
  const removeFromCart = function (productName) {
    const token = localStorage.getItem('token');

    if(!token){
      console.log('User is not authenticated.');
      return;
    }

    fetch("https://capstone2-alvaran.onrender.com/carts/removeProduct",{
      method: "DELETE",
      headers : {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        productName,
      }),
    })
      .then((res) => res.json())
      .then((data) =>{
        if(data) {
          console.log(`Product Removed from Cart`);
          Swal.fire({
            title: "Item sucessfully removed from Cart",
            icon: "success",
            text: "You have successfully removed this product from your cart",
          })
          fetchData();
        }else{
          Swal.fire({
            title: "Something went wrong!",
            icon: "error",
            text : "There was a problem while removing the product, please try again",
          })
        }
      })
      .catch((error) => {
        console.error("Error updating cart: ", error)
      })

  };
 
  //for Checking out
  const checkout = function (productName, quantity){
    const token = localStorage.getItem('token');

    if(!token){
      // Handle the case where the user is not authenticated
      console.log("User is not authenticated");
      return;
    }

    // Creates an array of products to checkout
    const productsToCheckout = [
      {
        productName : productName,
        quantity: quantity,
      }
    ];

    fetch('https://capstone2-alvaran.onrender.com/carts/checkout', {
      method: "POST",
      headers :{
        "Content-Type" : "application/json",
        Authorization : `Bearer ${token}`,
      },
      body: JSON.stringify({productsToCheckout}), //sending the array
    })
    .then((res) => res.json())
    .then((data) =>{
      if(data) {
        Swal.fire({
          title :"Checked out!",
          icon: "success",
          text : "You've successfully checked out this product",
          
        })
        fetchData();
      }else{
        Swal.fire({
          title :"Error",
          icon: "error",
          text : "Something went wrong",
        })
      }
    })
  } 

  return (
    <>
      <CartView
      // =>CartView
       removeFromCart = {removeFromCart}
       // => CartView
       cartItems = {cartItems}
       // => CartView => QuantityUpdater
       updateQuantity = {updateQuantity}
       // => CartView
       //
       checkout = {checkout}

       />

      {/*  <CourseCard
       checkout = {checkout}

       cartItems = {cartItems}
       /> */}

    </>
  );
} 

