import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
//import CourseCard from '../components/CourseCard';

export default function Home() {

    const data = {
    	/* textual contents*/
        title: "Sample E-Commerce API",
        content: "Divisoria, Baclaran, Ukay-ukay",
        /* buttons */
        destination: "/products",
        label: "Continue Shopping!"
    }

    return (
        <>
            <Banner data={data}/>
            <Highlights />
        </>
    )
}