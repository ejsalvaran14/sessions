import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function CourseView(){

	const {user} = useContext(UserContext);

	const {courseId} = useParams();

	const navigate = useNavigate();

	const [productName, setProductName] = useState("");
	const [productDescription, setProductDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);

	const enroll = () => {
		fetch(`https://capstone2-alvaran.onrender.com/carts/addCart`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productName: productName,
				// productDesccription: productDescription,
				quantity: quantity,
				// price: price,
			}),
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Successfully Enrolled!",
					icon: "success",
					text: "You have successfully enrolled for this course."
				})

				navigate("/products");
			}else{
				Swal.fire({
					title: "Something Went Wrong!",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
	}

	useEffect(() => {
		console.log(courseId);

		fetch(`https://capstone2-alvaran.onrender.com/products/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProductName(data.productName);
			setProductDescription(data.productDescription);
			setQuantity(1)
			setPrice(data.price);
		})

	}, [courseId])


	return(
		<Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{productName}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{productDescription}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>
                            

                            {
                            	user.id !== null ?
                            	<Button variant="primary" onClick={() => enroll(courseId)}>Add to Cart</Button>
                            	:
                            	<Link className="btn btn-danger btn-block" to="/login">Login to Purchase</Link>
                            }

                            
                        </Card.Body>        
                    </Card>
                </Col>
            </Row>
        </Container>

		
		
		)
}