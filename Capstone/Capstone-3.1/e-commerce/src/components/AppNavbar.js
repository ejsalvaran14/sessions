import { useState, useContext } from 'react';
import {Container, Row, Col, Form, Button} from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import "boxicons";
import {CourseSearch} from "./CourseSearch"




export default function AppNavbar() {

    // State to store the user information stored in the login page.
    // const [user, setUser] = useState(localStorage.getItem("access"));
    // console.log(user);

    const { user } = useContext(UserContext);

    return(
        <Navbar bg="light" expand="lg">
            <Container fluid >
                <Navbar.Brand  as={Link} to="/">E-Commerce</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        {/* <Form inline>
                        <Row>
                        <Col xs="auto">
                        <Form.Control
                        type="text"
                        placeholder="Search"
                        className=" mr-sm-2"
                        />
                        </Col>
                        <Col xs="auto">
                        <Button type="submit" bg ="danger">Search</Button>
                        </Col>
                        </Row>
                        </Form> */}
                        <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
                        {(user.id !== null) ? 

                                user.isAdmin 
                                ?
                                <>
                                    <Nav.Link as={Link} to="/addProduct">Add Product</Nav.Link>
                                    <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                                </>
                                :
                                <>
                                    
                                    <Nav.Link as={Link} to="/cart"><box-icon name='cart' >Cart</box-icon></Nav.Link>
                                    <Nav.Link as={Link} to="/profile"><box-icon name='user' >Profile</box-icon></Nav.Link>
                                    <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                                </>
                            : 
                                <>
                                    <Nav.Link as={Link} to="/login">Login</Nav.Link>
                                    <Nav.Link as={Link} to="/register">Register</Nav.Link>
                                </>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        )
}