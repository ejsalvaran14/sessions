import { Card, Button } from 'react-bootstrap';
import coursesData from '../data/coursesData';
import {useState} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2'

export default function CourseCard({courseProp}) {
	// Checks to see if the data was successfully passed
	//console.log(props);
	// Every component recieves information in a form of an object
	//console.log(typeof props);

	const { _id, productName, productDescription, price } = courseProp;

	const [quantity, setQuantity] = useState(1);

	const enroll = () => {
	  fetch(`https://capstone2-alvaran.onrender.com/carts/addCart`, {
		method: 'POST',
		headers: {
		  'Content-Type': 'application/json',
		  Authorization: `Bearer ${localStorage.getItem('token')}`,
		},
		body: JSON.stringify({
		  productName: productName,
		  quantity: quantity, 
		}),
	  })
		.then((res) => res.json())
		.then((data) => {
		  if (data) {
			Swal.fire({
			  title: 'Successfully Enrolled!',
			  icon: 'success',
			  text: 'You have successfully enrolled for this course.',
			});
		  } else {
			Swal.fire({
			  title: 'Something Went Wrong!',
			  icon: 'error',
			  text: 'Please try again!',
			});
		  }
		});
	};

	// Use the state hook in this component to be able to store its state
	/*
		Syntax:
			const [getter, setter] = useState(initialGetterValue);
	*/
	// const [count, setCount] = useState(0);

	// console.log(useState(0));

	// Use state hook for getting and setting the seats for this course
	// const [seats, setSeats] = useState(30);

	// Function that keeps track of the enrollees for a course
	// function enroll(){
	// 	if (seats > 0) {
	// 	    setCount(count + 1);
	// 	    console.log('Enrollees: ' + count);
	// 	    setSeats(seats - 1);
	// 	    console.log('Seats: ' + seats)
	// 	} else {
	// 	    alert("No more seats available");
	// 	};
	// }

	return (

		
	<Card>
	    <Card.Body>
	        <Card.Title>{productName}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{productDescription}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
	        <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
			<button className="btn btn-primary" onClick={enroll}>
			Add to Cart
			</button>

			  
			
	    </Card.Body>


	</Card>	
	)

}

// Check if the CourseCard component is getting the correct prop types
CourseCard.propTypes = {
	course: PropTypes.shape({
		productName: PropTypes.string.isRequired,
		productDescription: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}