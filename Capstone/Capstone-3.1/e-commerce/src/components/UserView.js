import React, { useState, useEffect } from 'react';
import CourseCard from './CourseCard';
import CourseSearch from './CourseSearch';


export default function UserView({coursesData}) {

    const [courses, setCourses] = useState([])

    useEffect(() => {
        const coursesArr = coursesData.map(course => {
            //only render the active courses
            if(course.isActive === true) {
                console.log(course.isActive)
                return (

                    <>
                    <CourseCard courseProp={course} key={course._id}/>
                    </>)
            } else {
                return <h1>No Result</h1>;
            }
        })

        //set the courses state to the result of our map function, to bring our returned course component outside of the scope of our useEffect where our return statement below can see.
        setCourses(coursesArr)

    }, [coursesData])

    return(
        <>
            <CourseSearch />
            { courses }
        </>
        )
}