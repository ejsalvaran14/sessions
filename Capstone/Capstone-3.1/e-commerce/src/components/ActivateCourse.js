import { useState } from "react";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function ActivateCourse({ course }) {
  const [courseId, setCourseId] = useState("");

  const activateCourse = (e, courseId) => {
    e.preventDefault();

    fetch(`https://capstone2-alvaran.onrender.com/products/${courseId}/activate`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // Update the course object's isActive property
        const updatedCourse = { ...course, isActive: true };
        setCourseId(updatedCourse);

        if (!data.isActive) {
          Swal.fire({
            title: "Activate Success!",
            icon: "success",
            text: "Course Successfully Activated!",
          });
        } else {
          Swal.fire({
            title: "Update Error!",
            icon: "error",
            text: "Please try again!",
          });
        }
      });
  };

  return (
    <>
      <Button variant="success" onClick={(e) => activateCourse(e, course)}>
        Archive
      </Button>
    </>
  );
}