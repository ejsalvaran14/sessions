import { Row, Col, Card } from 'react-bootstrap';
import {Reveal} from "./Reveal"

export default function Hightlights(){
	return (

		<Reveal>
			<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Buy</h2>
			        </Card.Title>
			        <Card.Text>
					The journey of a thousand smiles begins with a single purchase.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Sell</h2>
			        </Card.Title>
			        <Card.Text>
					Turning passion into profit, one sale at a time.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Advertise</h2>
			        </Card.Title>
			        <Card.Text>
					Bringing brands to life and dreams to reality, one ad at a time.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
		</Row>
		</Reveal>
		
	)
}