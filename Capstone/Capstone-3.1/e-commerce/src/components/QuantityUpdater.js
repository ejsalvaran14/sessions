import React, { useState } from "react";

export default function QuantityUpdater({
  productName,
  initialQuantity,
  onUpdate,
}) {
  const [quantity, setQuantity] = useState(initialQuantity);

  const handleIncrement = () => {
    const newQuantity = quantity + 1;
    setQuantity(newQuantity);
    onUpdate(productName, newQuantity); // Pass the new quantity to the onUpdate function
  };

  const handleDecrement = () => {
    if (quantity > 1) {
      const newQuantity = quantity - 1;
      setQuantity(newQuantity);
      onUpdate(productName, newQuantity); // Pass the new quantity to the onUpdate function
    }
  };

  return (
    <div>
      <button onClick={handleDecrement}>-</button>
      <span>{quantity}</span>
      <button onClick={handleIncrement}>+</button>
    </div>
  );
}