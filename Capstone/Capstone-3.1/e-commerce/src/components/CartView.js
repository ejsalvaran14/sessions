import {Card, Container, Button} from 'react-bootstrap'
import QuantityUpdater from './QuantityUpdater'

export default function CartView({cartItems, removeFromCart, updateQuantity, checkout}) {
    return (
        <Container className="mt-5">
      <h2>Your Cart</h2>
      {cartItems && cartItems.length > 0 ? (
        cartItems.map((item, index) => (
          <Card key={index} className="mb-3">
            <Card.Body>
              <Card.Title>{item.productName}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{item.productDescription}</Card.Text>
              <Card.Subtitle>Quantity:</Card.Subtitle>
              <Card.Text>{item.quantity}</Card.Text>
              {/* QuantityUpdater Component */}
              <QuantityUpdater
                productName={item.productName}
                initialQuantity={item.quantity}
                onUpdate={updateQuantity}
              />
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {item.price}</Card.Text>
              <Card.Subtitle>TotalPrice:</Card.Subtitle>
              <Card.Text>PhP {item.totalPrice}</Card.Text>
              <Button
                variant="danger"
                onClick={() => removeFromCart(item.productName)}//Calls removecart with productName
              >
                Remove from Cart
              </Button>
              <Button
                variant="primary"
                onClick={() => checkout(item.productName, item.quantity)} // calls checkout with productName
              >
                Checkout
              </Button>
            </Card.Body>
          </Card>
        ))
      )
    :
    (
      <p>Your cart is empty.</p>
    ) 
    }
    </Container>
    )
} 


//Testing
/* 
import React from 'react';
import { Card, Container, Button } from 'react-bootstrap';

export default function CartView({ cartItems, removeFromCart, updateQuantity }) {
  return (
    <Container className="mt-5">
      <h2>Your Cart</h2>
      {cartItems && cartItems.length === 0 ? (
        <p>Your cart is empty.</p>
      ) : (
        cartItems.map((item, index) => (
          <Card key={index} className="mb-3">
            <Card.Body>
              <Card.Title>{item.productName}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{item.productDescription}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {item.price}</Card.Text>
              <div className="mb-2">
                <input
                  type="checkbox"
                  id={`productCheckbox_${index}`}
                  // Add any necessary event handlers here
                />
                <label htmlFor={`productCheckbox_${index}`} className="ml-2">
                  Select this item
                </label>
              </div>
              <div className="d-flex align-items-center">
                <Button
                  variant="danger"
                  className="mr-2"
                  onClick={() => removeFromCart(item.productName)}
                >
                  Remove from Cart
                </Button>
                <div>
                  <button
                    onClick={() => updateQuantity(item.productName, item.quantity - 1)}
                    disabled={item.quantity === 1}
                  >
                    -
                  </button>
                  <span className="mx-2">{item.quantity}</span>
                  <button onClick={() => updateQuantity(item.productName, item.quantity + 1)}>
                    +
                  </button>
                </div>
              </div>
            </Card.Body>
          </Card>
        ))
      )}
    </Container>
  );
}
 */