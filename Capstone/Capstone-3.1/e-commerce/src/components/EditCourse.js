import {Button, Modal, Form} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import Swal from 'sweetalert2';

export default function EditCourse({course, fetchData}){

	// state for course id which will be used for fetching
	const [courseId, setCourseId] = useState("");

	// state for editcourse modal
	const [showEdit, setShowEdit] = useState(false);

	// useState for our form (modal)
	const [productName, setProductName] = useState("");
	const [productDescription, setProductDescription] = useState("");
	const [price, setPrice] = useState("");

	// function for opening the edit modal
	 
	const openEdit = (courseId) => {
		setShowEdit(true);
		//setCourseId(courseId);//try

		// to still get the actual data from the form
		fetch(`https://capstone2-alvaran.onrender.com/products/${courseId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data)
			setCourseId(data._id)
			setProductName(data.productName)
			setProductDescription(data.productDescription)
			setPrice(data.price)

			//fetchData();
		})
	}

	const closeEdit = () => {
		setShowEdit(false);
		setProductName("");
		setProductDescription("");
		setPrice(0);
	}

	// function to save our update
	const editCourse = (e, courseId) => {
		e.preventDefault();

		fetch(`https://capstone2-alvaran.onrender.com/products/${courseId}`, {
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productName: productName,
				productDescription: productDescription,
				price: price
			})
			})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Update Success!",
					icon: "success",
					text: "Course Successfully Updated!"
				})
				fetchData();
				closeEdit();
			}else{
				Swal.fire({
					title: "Update Error!",
					icon: "error",
					text: "Please try again!"
				})
				closeEdit();
			}

		})
	}

	


	

	return(
		<>
			<Button variant="primary" size="sm" onClick={() => {openEdit(course)}}>Edit</Button>

			 <Modal show={showEdit} onHide={closeEdit}>
                <Form onSubmit={e => editCourse(e, courseId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Course</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>    
                        <Form.Group controlId="productName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" required value={productName} onChange={e => setProductName(e.target.value)}/>
                        </Form.Group>

                        <Form.Group controlId="productDescription">
                            <Form.Label>Description:</Form.Label>
                            <Form.Control type="text" required value = {productDescription} onChange={e => setProductDescription(e.target.value)}/>
                        </Form.Group>

                        <Form.Group controlId="productPrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number" required value={price} onChange={e => setPrice(e.target.value)}/>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
		</>

		)
}